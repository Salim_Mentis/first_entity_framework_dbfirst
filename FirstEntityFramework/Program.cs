﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstEntityFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            //
            //Context class called
            var context = new CustomerEntities2();
            //Data Model Object added
            var post = new NewCountry()
            {
                ID = 1,
                CountryCode = "BE",
                CountryName = "Belgium"
            };
            context.NewCountries.Add(post);
            context.SaveChanges();
        }
    }
}
